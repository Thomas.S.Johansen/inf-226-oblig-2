import bcrypt
salt = bcrypt.gensalt()

def hashPassword(input: str) -> bytes:
    b = input.encode('utf-8')
    encrypted = bcrypt.hashpw(b, salt)
    return encrypted

def checkPassword(input_password: str, true_password: str ) -> bool:
    b = input_password.encode('utf-8')
    encrypted = (bcrypt.hashpw(b, salt)).decode("utf-8")
    return encrypted == true_password