import hashing_code as hash
import sys
import apsw
from apsw import Connection


def initialize_database(conn): 
    c = conn.cursor()
    #Messages
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY, 
        sender TEXT NOT NULL,
        receiver TEXT NOT NULL,
        message TEXT NOT NULL);''')
    #Announcements
    c.execute('''CREATE TABLE IF NOT EXISTS announcements (
        id INTEGER PRIMARY KEY, 
        author TEXT NOT NULL,
        text TEXT NOT NULL);''')
    #Drop table users since everytime i restart the code, it tries to create the same users
    #In a real scenario this whould not be a problem, since users are added dynamicaly, and not 
    #on startup
    c.execute('''DROP TABLE users''')
    #Users
    c.execute('''CREATE TABLE IF NOT EXISTS users (
        id INTEGER PRIMARY KEY,
        name TEXT NOT NULL,
        password TEXT NOT NULL,
        token TEXT);''')
    #Add Users 
    username = "alice"
    password = (hash.hashPassword("password123")).decode("utf-8")
    c.execute(f'''INSERT INTO users (id, name, password, token) values ('1','{username}','{password}', 'tiktok')''')
    username = "bob"
    password = (hash.hashPassword("bananas")).decode("utf-8")
    c.execute(f'''INSERT INTO users (id, name, password, token) values ('2','{username}','{password}', '')''')
    username = "thomas"
    password = (hash.hashPassword("secret")).decode("utf-8")
    c.execute(f'''INSERT INTO users (id, name, password, token) values ('3','{username}','{password}', '')''')
    