

# Considerations Part A

### Security Issues On Initial Inspection:

- The input boxes can be used for sql injection like so:  
Drop Message table:  
``` '); DROP TABLE messages; -- ```
    - This was easy to see, since the messages display the full  
    SQL query and not just the actual message
    - Therfore, im listing the way messages are displayed as another  
    security issue. 
- Loggin did not check password

### Considerations for refactoring
Looking at the code inn app.py, it appeared as somewhat of a scrambeled mess,  
and was intimidating to work with at first. Uppon firther inspection,  
 things started to make more sence, but it's still a bit messy.
 It was at this point two possible paths laid themself bare:  
 Either, I meticiously read all the code and figure out how to make it better.  
 Or, i make sure everything i add to the code has better structure, and leave  
  the mess mostly as is.   
  I essentially went for the second option. :) 

The html code prints the full mysql statement in messages.
While this whould normaly probably be a security issue, I identified  
the relevant part of the code in case i wanted to change it, but left it  
for now to easier see the query that was parsed. 
  
<br><br><br>

# Features
- If you have a valid username and password, you can logg inn
- While logged inn, you can write a message, to one or more people.
- You can send said message.
- You can see messages send by you and too you by serching or viewing all messages. 
-  You can log out.
###### This seems depressingly simple when summarized like this :)

<br><br><br>

# Instructions
To log inn, you can use one of the tree built inn users  
- alice - password123  
- bob - banana  
- thomas - secret

Username and receipient are case sensitive.   
Search seems to only work if you spell out the co 
mplete content of a message, don't know if it was  
always like that, or if i broke it.   
Show all will show all messages the current user has sent or received.   
From only affects who the sender is in the query confirmation message,   
but is not sent to the actuall database.  
one or more receipients can be added in the `To: `field.   
They need to be separated by a `,` and no spaces.   
In the message box you write whatever you fancy.  
It should hopefully be sanitized.  
The `logout` button will (obviusly) log you out. 
<br><br><br>


# Technical Details

## Refactoring:
### Unused code:
``` 
@app.get('/coffee/')
def nocoffee():
    abort(418)

@app.route('/coffee/', methods=['POST','PUT'])
def gotcoffee():
    return "Thanks!"
``` 
As far as i can tell, no other method calls these coffie functions,  
 thus i have declared them useless and removed them.
  
### Code related to hasing is placed in the hashing_code.py file  
 - The hasing_code file has two methods
    -Hash password takes a string and returns  
    the hashed password using the previusly generated salt
 - The check password takes inn two strings,  
   a plaintext input password, and a hashed password  
   it then hashes the plaintext password and returns true if they match.  

### Code related to creating the database is placen in the database.py file
- This file creates the initial tables, and also for the purpose of this   
test server adds the users. In a propper setting, there whould be some add user  
functionality on the website login form. 

### The index.html file
- Added a logout button, using the ``` flask_login.logout_user() ``` command
- The ```From: ``` text field has no practical use anymore,  
it is only used for message headlines, not sendt to the database
- Added the `To:` field, used to determine who gets a message
- Added a reply button to each mesasge, it however has no usage as of now
- Added ghost text in boxes

### Main app.py file
- Moved database initializing code over to the `database.py` file.
    - To compensate for this, there is now a main sort of function,  
    which creates the conn aspw conneciton and stores it as a global variable. 
    - This main also imports `database.py` and `hashing_code.py`  
- The `users` dictionary in the original code is now replaced with the `getUsers`   
method. Since username and password is now stored in a database, this method retrieves it.  
To be compatible with the rest of the code, it also organizes it into a equally structured dict.  This way, all methods that before called `users` now call `getUsers()` instead. 
- The `split_recievers` method takes in a string and splits it into a list on commas.  
This is used in the `send` method
- The `app.secret_key` is now a random generated string each time the code restarts.  
Have not touched that in any other way.
- The `Loggin`method now makes use of the `hashing_code`to check if the input password is  
correct compared to the hashed password in the database. 
- Added `logout`method to logout the user.
- `Search` and `Send` have both had their user inputs "sanitized" for sql injections by 
using prepared statements. 
- `Send` now functions diffrently than before. If there is more  
than one receipient, there is sent one message for each. This is  
so that there will only be one receipient entry per message in the database,  
which helps the `Search` funtion, and I also have a vague memory  
from INF115 that having only one entry per cell is important. 
###### Again, this felt like alot more when I did it then when im summarizing it :)
<br><br><br>

# Answeres

## Threat model
A attacker whould be someone that wants to read other peoples messages,  
or send messages where they pretend to be someone else   
either to send links to malware or manipulate in som other way. 
The app is limited to text messages only, so sending direct infected files or images would  
not be as easy than say in `Discord`. 
The passwords are hashed and salted, to even with access to the database, the hacker whould  
still need to essentially brute force the passwords to access the site trough normal means.  
Testing the ` http://localhost:5000/search?q=*` query in searchbar trick did not seem to work, as the server responded that a user was missing. Hopefully this means that HTTP requests are protected against.   
If some register user feature existed, then a attacker whould have a way to get past the first barrier. In this case, since the user inputs are sanetized, there is no obvius way to get another persons messages.   
There is however the threat of cross-site scripting attacks or request forgery.  
In the cases of these, I have not implemented any direct countermasures,  
so we are relying on the html not being too badly written, and on packages and similar  
being up to date.
<br>

## Main attack vectors

I would assume the main vectors here lies in tampering with cookies or the html to   
manipulate the appliation, since i have not implemented any direct counters to this.  
The app.py file still has alot of responsibility, so access to this file chould give  
an attacker alot of insight in how to attack the system. 


<br>

## What have you done?
Implemented the loggin now using passwords.  
Passwords are stored hased with salt, altough not in a sepparate table than usernames.   
Sanitized the user inputs from sql injections.   
Users can only see their own messages.  

<br>

## What is the access control model?
Access control are ways to authorize access to a system. There are three main forms of access controlls, Mandatory Access Control (MAC), Discretionarey Access Control (DAC) and Role Based Access Control (RBAC).
- MAC provides the most restrictive protections, where all system access falls is given by system administrators. Users cannot change premissions that deny or allow them to access different areas, giving high security to sensitive informations.
- DAC puts a little more control back into the hands of the systems owner. They can determine who gets to acces what, regardless of any admin created hirearchy. One only needs the right credentials to access something. One downside is that giving an end user such control can lead to oversights on their part, which again leads to security issues. 
- RBAC are role based premissions to a user based on their responsibilities in the system. Emplyees are spesifically given access to what they need. This method is the most flexible model, and maintains protection against breaches and data leaks.  


<br>

## HOW CAN YOU KNOW IT'S GOOD ENOUGH??
### I can't, theres always some way to mess it up :)





